

class PaginatorMixin(object):
    page_size = 0
    max_page_size = 100

    def __init__(self):
        pass

    def is_page_size_large_to_max_size(self):
    	return self.page_size >= self.max_page_size

    def increase_page_size(self):
    	self.page_size = self.page_size + 1

