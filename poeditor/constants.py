# -*- coding: utf-8 -*-
LANGUAGES = {
    'de': u'Deutsch',
    'tr': u'Türk',
    'en': u'English',
    'nl': u'Nederlands',
    'fr': u'русский',
    'ru': u'Russian',
    'gr': u'ελληνικά',
    'sp': u'español',
    'it': u'italiano',
}
LANGUAGES_VAL = {
    u'Deutsch':'de',
    u'Türk':'tr',
    u'English':'en',
    u'Nederlands':'nl',
    u'русский':'fr',
    u'Russian':'ru',
    u'ελληνικά':'gr',
    u'español':'sp',
    u'italiano':'it',
}
GROUPS = (
    ('all',u'all'),
    ('mobile', u'mobile'),
    ('web', u'web'),
    ('device', u'device'),
    ('print', u'print'),
    ('others', u'others'),
)