import redis
import pickle


class RedisCache(object):

	def __init__(self):
		self._host = 'localhost'

	def _initialize_collection(self):
		if self._host :
			self.connection = redis.Redis(self._host)
			return self.connection


	def set_cache(self,key,value):
		r_con = self._initialize_collection()
		if r_con:
			pickle_value = pickle.dumps(value)
			r_con.set(key,pickle_value)
			return True
		else:
			return False

	def get_cache(self,key,default=None):
		r_con = self._initialize_collection()
		if r_con:
			value = r_con.get(key)
			if value:
				pickle_value = pickle.loads(value)
			else :
				pickle_value = None
			return pickle_value
		else:
			return default

redis_cache = RedisCache()