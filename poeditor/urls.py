from django.contrib import admin
from django.conf.urls import patterns, include, url


from poeditor_app.views import panel_page


urlpatterns = patterns('',
    url(r'^$',panel_page,name='panel_page'),
    url(r'^poeditor/', include('poeditor_app.urls',namespace='poeditor_app')),
    url(r'^admin/', include(admin.site.urls)),
)
