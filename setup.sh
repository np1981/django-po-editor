#!/bin/bash
echo "poeditor Project setup.."

cd django-po-editor

pip install virtualenv
virtualenv venv
source venv/bin/activate

#Install Project Requrements
pip install -r requirements.txt

echo "successfully install poeditor Project.."