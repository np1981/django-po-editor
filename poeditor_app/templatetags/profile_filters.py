import re


from django.template import Library

 
register = Library()
 

@register.filter(name='placeholder')
def placeholder(value, token):
    value.field.widget.attrs["placeholder"] = token
    return value
 
@register.filter(name='hiddenfiled')
def hiddenfiled(value):
    try:
        value.field.widget.input_type="hidden"
    except Exception, e:
        value.base_fields.widget.input_type="hidden"
    return value   

@register.filter(name='addclass')
def addclass(value, arg):
   return value.as_widget(attrs={'class': arg})  
