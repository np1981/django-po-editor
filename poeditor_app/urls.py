from django.conf.urls import patterns, url


from poeditor_app.views import edit_panel_page,search_panel_page


urlpatterns = patterns('',
  url(r'^edit/panel/page/$',edit_panel_page,name='edit_panel_page'),
  url(r'^search/panel/page/$',search_panel_page,name='search_panel_page'),
)

