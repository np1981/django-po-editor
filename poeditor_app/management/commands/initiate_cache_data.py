import six
import polib
import hashlib
from itertools import combinations


from django.conf import settings
from django.core.management.base import BaseCommand, CommandError


from poeditor_app.forms import PanelPageForm
from poeditor.redis_cache import redis_cache


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        cache_id_data = {}
        cache_language_data = {}
        cache_id_ocurrence = {}
        cache_id_list = []
        count = 1
        Ocurrenc_group = {"web":set(),"mobile":set(),"device":set(),"print":set(),"others":set(),"all":set()}

        for leng in settings.LANGUAGES:
            print "===================LANGUAGES==================="
            print leng[0]
            po = polib.pofile('poeditor_app/locale/%s/LC_MESSAGES/django.po'%leng[0])
            for entry in po:
                if "#~ msgid" in str(entry):
                    continue
                entry.md5hash = hashlib.md5((six.text_type(entry.msgid) +six.text_type(entry.msgstr) +six.text_type(entry.msgctxt or "")).encode('utf8')).hexdigest()
                if six.text_type(entry.msgid):
                    is_fuzzy = False
                    for flag in entry.flags:
                        if flag == "fuzzy":
                            is_fuzzy = True
                    print "msgid"
                    print six.text_type(entry.msgid)
                    if not entry.obsolete:
                        flag = False
                        for occurrences in entry.occurrences:
                            Ocurrenc_group['all'].add(six.text_type(entry.msgid))
                            if "web" in occurrences[0]:
                                Ocurrenc_group['web'].add(six.text_type(entry.msgid))
                                flag = True
                            if "mobile" in occurrences[0]:
                                Ocurrenc_group['mobile'].add(six.text_type(entry.msgid))
                                flag = True
                            if "device" in occurrences[0]:
                                Ocurrenc_group['device'].add(six.text_type(entry.msgid))
                                flag = True
                            if "print" in occurrences[0]:
                                Ocurrenc_group['print'].add(six.text_type(entry.msgid))
                                flag = True

                        if not flag :
                            Ocurrenc_group['others'].add(six.text_type(entry.msgid))

                    if not cache_language_data:
                        cache_language_data = { six.text_type(entry.msgid) :[{"language":leng[0],"str":six.text_type(entry.msgstr),"is_fuzzy":is_fuzzy}]}
                        if not cache_id_data:
                            cache_id_data = { count :six.text_type(entry.msgid) }
                            cache_id_ocurrence = { count : entry.occurrences }
                        else:
                            cache_id_data[count] = six.text_type(entry.msgid)
                            cache_id_ocurrence[count] = entry.occurrences 
                        if not cache_id_list:
                            cache_id_list = [count]
                        else :
                            cache_id_list.append(count)
                        count = count +1
                    else:
                        try:
                            if {"language":leng[0],"str":six.text_type(entry.msgstr),"is_fuzzy":is_fuzzy} not in cache_language_data[six.text_type(entry.msgid)]:
                                cache_language_data[six.text_type(entry.msgid)].append({"language":leng[0],"str":six.text_type(entry.msgstr),"is_fuzzy":is_fuzzy})
                        except KeyError,e :
                            cache_language_data[six.text_type(entry.msgid)] = [{"language":leng[0],"str":six.text_type(entry.msgstr),"is_fuzzy":is_fuzzy}]
                            if not cache_id_data:
                                cache_id_data = { count :six.text_type(entry.msgid) }
                                cache_id_ocurrence = { count : entry.occurrences }
                            else:
                                cache_id_data[count] = six.text_type(entry.msgid)
                                cache_id_ocurrence[count] = entry.occurrences 
                            if not cache_id_list:
                                cache_id_list = [count]
                            else :
                                cache_id_list.append(count)
                            count = count +1


        redis_cache.set_cache(settings.CACHE_LANGUAGES,cache_language_data)
        redis_cache.set_cache(settings.CACHE_IDS,cache_id_data)
        redis_cache.set_cache(settings.CACHE_OCURRENCE,cache_id_ocurrence)
        redis_cache.set_cache(settings.CACHE_GROUP,Ocurrenc_group)
        redis_cache.set_cache(settings.CACHE_IDS_LIST,cache_id_list)


        print "------------------------CACHE_IDS -------------------------------"
        print redis_cache.get_cache(settings.CACHE_IDS)
        print "-----------------------------------------------------------------"
        print "------------------------CACHE_LANGUAGES--------------------------"
        print redis_cache.get_cache(settings.CACHE_LANGUAGES)
        print "-----------------------------------------------------------------"
        print "------------------------CACHE_OCURRENCE--------------------------"
        print redis_cache.get_cache(settings.CACHE_OCURRENCE)
        print "-----------------------------------------------------------------"
        print "------------------------CACHE_GROUP------------------------------"
        print redis_cache.get_cache(settings.CACHE_GROUP)
        print "-----------------------------------------------------------------"
        print "------------------------CACHE_IDS_LIST---------------------------"
        print redis_cache.get_cache(settings.CACHE_IDS_LIST)
        print "-----------------------------------------------------------------"