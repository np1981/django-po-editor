��          t      �              #   '     K  
   f  �   q  $   6  E   [     �  ]   �       �  (     �  )   �     �       �   *  '   	  M   1       [   �     �     	                                      
                 1 Team One - Team Two Error by installing printer drivers Installing printer drivers No program Printer driver is not yet installed on this system. Should printer drivers be installed (needed for ticket printing)? \\n\\nYou should have administration rights on this system for this operation. Printer drivers installed successful Printer drivers installed successful. Please restart the application. Printing operation failed! Unfortunately, during installation errors have been occurred. Please restart the application. not specified Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-05-15 09:56+0200
PO-Revision-Date: 2015-02-11 12:17+0100
Last-Translator: Cali Mero <admin2@technic.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Translated-Using: django-rosetta 0.7.2
 1 Team Eins - Team Zwei Fehler bei der Druckertreiberinstallation Druckertreiber installieren Kein Spielbeginn Der Druckertreiber ist auf dem System noch nicht installiert, soll der Treiber installiert werden (n\u00F6tig f\u00FCr Ticketdruck)? \n\nSie m\u00FCssen f\u00FCr die Aktion Administratorrechte auf dem System verf\u00FCgen. Druckertreiberinstallation vollständig Druckertreiberinstallation vollständig. Bitte starten die die Anwendung neu. Druckvorgang fehlgeschlagen! Leider sind bei der Installation Fehler aufgetreteten. Bitte starten die die Anwendung neu. Not Specified 