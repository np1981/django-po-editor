# -*- coding: utf-8 -*-
import polib


from django.conf import settings
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


from poeditor import constants
from poeditor.mixin import PaginatorMixin
from poeditor.redis_cache import redis_cache
from poeditor_app.forms import PanelPageForm,EditPanelPageForm,SearchPanelPageForm


class PanelPage(View,PaginatorMixin):

    def get(self, request, *args, **kwargs):
        response = {}
        filled_languages = request.GET.get('filled_languages')
        unfilled_languages = request.GET.get('unfilled_languages')
        groups = request.GET.get('groups')
        searchtext = request.GET.get('searchtext')
        print "searchtext",searchtext

        is_search = False
        if searchtext :
            searchtext = searchtext.lower()
            is_search = True

        print "is_search",is_search

        if not filled_languages:
            filled_languages = "--"
        if not unfilled_languages:
            unfilled_languages = "--"
        if not groups :
            groups = "all"

        if filled_languages and unfilled_languages and filled_languages == unfilled_languages :
            unfilled_languages = "--"
            filled_languages = "--"

        panel_form = PanelPageForm()
        panel_form.fields['filled_languages'].initial = filled_languages
        panel_form.fields['unfilled_languages'].initial = unfilled_languages
        panel_form.fields['groups'].initial = groups
        panel_form.fields['searchtext'].initial = searchtext

        cache_id_data = redis_cache.get_cache(settings.CACHE_IDS)
        cache_language_data = redis_cache.get_cache(settings.CACHE_LANGUAGES)
        cache_id_ocurrence = redis_cache.get_cache(settings.CACHE_OCURRENCE)
        cache_group_data = redis_cache.get_cache(settings.CACHE_GROUP)
        cache_id_list = redis_cache.get_cache(settings.CACHE_IDS_LIST)

        group_data = cache_group_data[groups]

        response['po_data'] = []
        response['page_size'] = []
        is_first = True
        first_key = ""
        last_key = ""
        page_number = 1

        if not is_search:
            for key in cache_id_list:
                value = cache_id_data[key]
                fill_flag = False
                unfill_flag = False
                for dicts in cache_language_data[value]:

                    dummy_filled_languages = filled_languages
                    if filled_languages == "--":
                        dummy_filled_languages = dicts['language']

                    dummy_unfilled_languages = unfilled_languages
                    if unfilled_languages == "--":
                        dummy_unfilled_languages = dicts['language']

                    if dummy_filled_languages == dicts['language'] and dicts['str'] and value in group_data:
                        fill_flag = True
                    if dummy_unfilled_languages == dicts['language'] and not dicts['str'] and value in group_data:
                        unfill_flag = True

                    if fill_flag and unfill_flag :
                        ocurrences = cache_id_ocurrence[key]
                        trans_lang = []
                        for lang in cache_language_data[value]:
                            if lang['str'] :
                                trans_lang.append(constants.LANGUAGES[lang['language']])
                        if {'id':key,'str':value,'trans_lang':trans_lang,"ocurrences":ocurrences} not in response['po_data']:
                            response['po_data'].append({'id':key,'str':value,'trans_lang':trans_lang,"ocurrences":ocurrences})
                            last_key = key
                            if is_first :
                                first_key = key
                                is_first = False
                            self.increase_page_size()
                            if self.is_page_size_large_to_max_size():
                                self.page_size = 0
                                response['page_size'].append([page_number,first_key,key])
                                page_number = page_number + 1
                                is_first = True

        else:
            for key in cache_id_list:
                value = cache_id_data[key]
                fill_flag = False
                unfill_flag = False
                for dicts in cache_language_data[value]:
                    if searchtext in dicts['str'].lower() or searchtext in value.lower():

                        dummy_filled_languages = filled_languages
                        if filled_languages == "--":
                            dummy_filled_languages = dicts['language']

                        dummy_unfilled_languages = unfilled_languages
                        if unfilled_languages == "--":
                            dummy_unfilled_languages = dicts['language']

                        if dummy_filled_languages == dicts['language'] and dicts['str'] and value in group_data:
                            fill_flag = True
                        if dummy_unfilled_languages == dicts['language'] and not dicts['str'] and value in group_data:
                            unfill_flag = True

                        if fill_flag and unfill_flag:
                            ocurrences = cache_id_ocurrence[key]
                            trans_lang = []
                            for lang in cache_language_data[value]:
                                if lang['str'] :
                                    trans_lang.append(constants.LANGUAGES[lang['language']])
                            if {'id':key,'str':value,'trans_lang':trans_lang,"ocurrences":ocurrences} not in response['po_data']:
                                response['po_data'].append({'id':key,'str':value,'trans_lang':trans_lang,"ocurrences":ocurrences})
                                last_key = key
                                if is_first :
                                    first_key = key
                                    is_first = False

                                self.increase_page_size()
                                if self.is_page_size_large_to_max_size():
                                    self.page_size = 0
                                    response['page_size'].append([page_number,first_key,key])
                                    page_number = page_number + 1
                                    is_first = True



        response['page_size'].append([page_number,first_key,last_key])
        response['panel_form'] = panel_form

        search_form = SearchPanelPageForm()
        search_form.fields['group'].initial = groups
        search_form.fields['fill_language'].initial = filled_languages
        search_form.fields['unfill_languages'].initial = unfilled_languages
        search_form.fields['search_text'].initial = searchtext
        response['search_form'] = search_form

        if groups == "all":
            response['groups'] = ""
        else:
            response['groups'] = groups

        try:
            response['load_key'] = response['page_size'][0][2]
        except Exception, e:
            response['load_key'] = response['po_data'][-1]["id"]

        return render(request, 'poeditor_app/panel-html.html', response)

panel_page = PanelPage.as_view()


class EditPanelPage(View):

    def get(self, request, *args, **kwargs):
        response ={}
        cache_language_data = redis_cache.get_cache(settings.CACHE_LANGUAGES)
        unfilled_language = request.GET.get('unfilled_language')
        filled_languages = request.GET.get('filled_language')
        submit_language = request.GET.get('submit_language')
        if submit_language :
            unfilled_language = constants.LANGUAGES_VAL[submit_language]
        languages = settings.DEFAULT_LANGUAGE

        if unfilled_language == "--":
            if filled_languages == "--":
                languages = settings.DEFAULT_LANGUAGE
            else:
                languages = filled_languages
        else :
            languages = unfilled_language
        
        strid = request.GET.get('str')
        select_group = request.GET.get('select_group')
        id_data_list = cache_language_data[strid]

        edit_form = EditPanelPageForm()
        edit_form.fields['groups'].initial = select_group  
        edit_form.fields['strid'].initial = strid  
        edit_form.fields['unfilled_languages'].initial = unfilled_language  
        edit_form.fields['filled_languages'].initial = filled_languages                 
        
        cache_language_data = redis_cache.get_cache(settings.CACHE_LANGUAGES)
        fuzzy_language = cache_language_data[strid]
        for fuzzy in fuzzy_language:
            if fuzzy['language'] == languages and fuzzy['is_fuzzy'] :
                edit_form.fields['fuzzy'].initial = True

        response['language'] = []
        for lang in  settings.LANGUAGES:
            id_str = ""
            for l in id_data_list:
                if l['language'] == lang[0]:
                    id_str = l['str']
            if lang[0] == languages:
                response['language'].append({"lang":lang[1],"select":True,"str":id_str})
                edit_form.fields['information'].initial = id_str
            else:
                response['language'].append({"lang":lang[1],"str":id_str})
        if request.GET.get('errors') :
            response['errors'] = request.GET.get('errors')
        response['select_group'] = select_group
        response['edit_form'] = edit_form
        return render(request, 'poeditor_app/edit-page.html', response)


    def post(self, request, *args, **kwargs):
        cache_language_data = redis_cache.get_cache(settings.CACHE_LANGUAGES)
        edit_form = EditPanelPageForm(request.POST) 
        if edit_form.is_valid():
            information = edit_form.cleaned_data['information']
            groups = edit_form.cleaned_data['groups']
            strid = edit_form.cleaned_data['strid']
            unfilled_language = edit_form.cleaned_data['unfilled_languages']
            filled_languages = edit_form.cleaned_data['filled_languages']
            fuzzy = edit_form.cleaned_data['fuzzy']

            languages = settings.DEFAULT_LANGUAGE

            if unfilled_language == "--":
                if filled_languages == "--":
                    languages = settings.DEFAULT_LANGUAGE
                else:
                    languages = filled_languages
            else :
                languages = unfilled_language

            id_data_list = cache_language_data[strid]
            is_save = False
            for lang in id_data_list:
                if lang['language'] == languages:
                    lang['str'] = information
                    lang['is_fuzzy'] = fuzzy
                    po = polib.pofile('poeditor_app/locale/%s/LC_MESSAGES/django.po'%lang['language'])
                    for entry in po:
                        is_old_fuzzy = False
                        for flag in entry.flags:
                            if flag == "fuzzy":
                                is_old_fuzzy = True

                        if entry.msgid == strid:
                            is_save = True
                            if fuzzy :
                                if not is_old_fuzzy:
                                    entry.flags.append("fuzzy")
                            else:
                                if is_old_fuzzy:
                                    entry.flags.remove("fuzzy")

                            entry.msgstr = information
                    po.save()                    
            redis_cache.set_cache(settings.CACHE_LANGUAGES,cache_language_data)
            if not is_save:
                return HttpResponseRedirect('/poeditor/edit/panel/page/?str=%s&filled_language=%s&unfilled_language=%s&select_group=%s&errors=%s'%(strid,filled_languages,unfilled_language,groups,'"%s" is not in %s language PO file'%(strid,constants.LANGUAGES[languages])))
        else :
            strid = request.POST.get("strid")
            filled_languages = request.POST.get("filled_languages")
            unfilled_language = request.POST.get("unfilled_languages")
            groups = request.POST.get("groups")

        return HttpResponseRedirect('/poeditor/edit/panel/page/?str=%s&filled_language=%s&unfilled_language=%s&select_group=%s'%(strid,filled_languages,unfilled_language,groups))
edit_panel_page = EditPanelPage.as_view()


class SearchPanelPage(View,PaginatorMixin):

    def get(self, request, *args, **kwargs):
        response = {}
        search_form = SearchPanelPageForm(request.GET)
        filled_languages = request.GET.get('fill_language')
        unfilled_languages = request.GET.get('unfill_languages')
        groups = request.GET.get('group')

        if filled_languages and unfilled_languages and filled_languages == unfilled_languages :
            unfilled_languages = "--"
            filled_languages = "--"

        submit_language = request.GET.get('submit_language')
        if submit_language :
            unfilled_language = constants.LANGUAGES_VAL[submit_language]


        search_text = ""

        if not filled_languages:
            filled_languages ="de"

        if not groups:
            groups = "all"

        panel_form = PanelPageForm()
        panel_form.fields['filled_languages'].initial = filled_languages
        panel_form.fields['unfilled_languages'].initial = unfilled_languages
        panel_form.fields['groups'].initial = groups


        if search_form.is_valid():
            search_text = search_form.cleaned_data['search_text']

            cache_id_data = redis_cache.get_cache(settings.CACHE_IDS)
            cache_language_data = redis_cache.get_cache(settings.CACHE_LANGUAGES)
            cache_id_ocurrence = redis_cache.get_cache(settings.CACHE_OCURRENCE)
            cache_group_data = redis_cache.get_cache(settings.CACHE_GROUP)
            cache_id_list = redis_cache.get_cache(settings.CACHE_IDS_LIST)

            group_data = cache_group_data[groups]


            search_form = SearchPanelPageForm()
            search_form.fields['group'].initial = groups
            search_form.fields['fill_language'].initial = filled_languages
            search_form.fields['unfill_languages'].initial = unfilled_languages
            search_form.fields['search_text'].initial = search_text

            response['po_data'] = []
            response['page_size'] = []
            is_first = True
            first_key = ""
            last_key = ""
            page_number = 1
            search_text = search_text.lower()
            for key in cache_id_list:
                value = cache_id_data[key]
                fill_flag = False
                unfill_flag = False
                for dicts in cache_language_data[value]:
                    if search_text in dicts['str'].lower() or search_text in value.lower():

                        dummy_filled_languages = filled_languages
                        if filled_languages == "--":
                            dummy_filled_languages = dicts['language']

                        dummy_unfilled_languages = unfilled_languages
                        if unfilled_languages == "--":
                            dummy_unfilled_languages = dicts['language']

                        if dummy_filled_languages == dicts['language'] and dicts['str'] and value in group_data:
                            fill_flag = True
                        if dummy_unfilled_languages == dicts['language'] and not dicts['str'] and value in group_data:
                            unfill_flag = True

                        if fill_flag and unfill_flag:
                            ocurrences = cache_id_ocurrence[key]
                            trans_lang = []
                            for lang in cache_language_data[value]:
                                if lang['str'] :
                                    trans_lang.append(constants.LANGUAGES[lang['language']])
                            if {'id':key,'str':value,'trans_lang':trans_lang,"ocurrences":ocurrences} not in response['po_data']:
                                response['po_data'].append({'id':key,'str':value,'trans_lang':trans_lang,"ocurrences":ocurrences})
                                last_key = key
                                if is_first :
                                    first_key = key
                                    is_first = False

                                self.increase_page_size()
                                if self.is_page_size_large_to_max_size():
                                    self.page_size = 0
                                    response['page_size'].append([page_number,first_key,key])
                                    page_number = page_number + 1
                                    is_first = True

            response['page_size'].append([page_number,first_key,last_key])


        panel_form.fields['searchtext'].initial = search_text
        response['panel_form'] = panel_form
        
        response['search_form'] = search_form

        try:
            response['load_key'] = response['page_size'][0][2]
        except Exception, e:
            response['load_key'] = response['po_data'][-1]["id"]

        if groups == "all":
            response['groups'] = ""
        else:
            response['groups'] = groups
        return render(request, 'poeditor_app/panel-html.html', response)

search_panel_page = SearchPanelPage.as_view()