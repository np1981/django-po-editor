# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings


from poeditor import constants


class PanelPageForm(forms.Form):
    LANGUAGES = (('--', u'-------'),) + settings.LANGUAGES
    submit_type = forms.CharField(max_length=20)
    filled_languages = forms.ChoiceField(choices=LANGUAGES,required=False)
    unfilled_languages = forms.ChoiceField(choices=LANGUAGES,required=False)
    groups = forms.ChoiceField(choices=constants.GROUPS,required=False)
    searchtext = forms.CharField(max_length=100)

class EditPanelPageForm(forms.Form):
    groups = forms.ChoiceField(choices=constants.GROUPS, required=False, widget=forms.Select(attrs={'disabled':'true', 'readonly':'readonly'}))
    information = forms.CharField(widget=forms.Textarea)
    strid=forms.CharField(widget=forms.TextInput(attrs={'class':'disabled', 'readonly':'readonly'}))
    unfilled_languages = forms.CharField(max_length=20, required=False)
    filled_languages = forms.CharField(max_length=20, required=False)
    fuzzy = forms.BooleanField(required=False)

class SearchPanelPageForm(forms.Form):
    search_text = forms.CharField(max_length=100)
    fill_language = forms.CharField(max_length=50)
    unfill_languages = forms.CharField(max_length=50)
    group = forms.CharField(max_length=50)
