$("#fill_unfill_submit_form").click(function(){
  this.form.submit()
});

$('#id_filled_languages').change(function() {
/*    $('#id_submit_type').val("filled_languages");
*/    
    $("#id_fill_language").val($(this).val());
   /* this.form.submit();*/
});

$('#id_unfilled_languages').change(function() {

    $("#id_unfill_languages").val($(this).val());
});

$('#id_groups').change(function() {
    $("#id_group").val($(this).val())
});

$('.edit_button').on('click', function(){
    this.form.submit();
});

$('#bt_search').on('click', function(){
    if($("#id_search_text").val()){
      this.form.submit();
    }
    else{
      $("#fill_unfill_submit_form").parents('form').submit();
    }
});

$('#id_search_text').change(function() {
    $("#id_searchtext").val($(this).val())
    console.log($(this).val());
});

$(window).keydown(function(event){
  if( event.keyCode == 13 ) {
    event.preventDefault();
    if($("#id_search_text").val()){
      $("#id_search_text").parents('form').submit();
    }else{
      $("#id_searchtext").val("");
      $("#fill_unfill_submit_form").parents('form').submit();
  }
  }
});

$('input[name="unfilled_language"]').val($('#id_unfilled_languages').val());
$('input[name="filled_language"]').val($('#id_filled_languages').val());
$('input[name="select_group"]').val($('#id_groups').val());